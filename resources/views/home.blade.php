@extends('layout')

@section('titulo') 
 {{-- parametro para el titulo de cada vista  --}}
 Este es el home wiiii
@endsection
 
@section('content') 
{{-- <!-- parametro para el contenido de cada vista --> --}}

<div class="row justify-content-center">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active"> <h2>Primera</h2>
          <img src="img\michiemputado.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item"> <h2>Segunda</h2>
          <img src="img\veroEsperanding.png" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item"> <h2>Tercera</h2>
          <img src="img\unmeco.gif" class="d-block w-100" alt="...">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

</div>

@endsection
